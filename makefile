CXX = g++
CXXFLAGS = -g -Wall -Wextra -Wpedantic

.PHONY : all clean
all : reachable

reachable : reachable.cpp graph.o
	$(CXX) $(CXXFLAGS) -o $@ $^

graph.o : graph.cpp graph.h
	$(CXX) $(CXXFLAGS) -c $<

clean :
	$(RM) reachable *.o

